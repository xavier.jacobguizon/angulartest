import { Component, OnInit } from '@angular/core';
import { TextService } from '../../service/text.service';



@Component({
  selector: 'app-textlist',
  templateUrl: './textlist.component.html',
  styleUrls: ['./textlist.component.scss']
})
export class TextlistComponent implements OnInit{
  public newText: string = '';
  public textList: string[] = [];

  constructor(private textService: TextService) {}

  ngOnInit() {
    this.textService.getTextList().subscribe((textList: string[]) => {
      this.textList = textList;
    });
  }

  public addText(): void {
    this.textService.addText(this.newText);
    this.newText = '';
  }

}


