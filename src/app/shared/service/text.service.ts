import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TextService {

  private textList: BehaviorSubject<string[]> = new BehaviorSubject<string[]>([]);

  constructor() { }

  public addText(text: string): void {
    const currentList = this.textList.value;
    currentList.push(text);
    this.textList.next(currentList);
  }

  public getTextList(): BehaviorSubject<string[]> {
    return this.textList;
  }

}

