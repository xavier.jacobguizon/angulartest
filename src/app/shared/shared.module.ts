import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TextlistComponent } from './component/textlist/textlist.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppComponent } from '../app.component';
import { TextService } from './service/text.service';



@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule
  ],
  declarations: [
    TextlistComponent
  ],
  exports: [
    TextlistComponent
  ],
  providers: [
    TextService
  ]
})
export class SharedModule { }
